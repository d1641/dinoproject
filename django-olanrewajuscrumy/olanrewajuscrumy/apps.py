from django.apps import AppConfig


class OlanrewajuscrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'olanrewajuscrumy'
