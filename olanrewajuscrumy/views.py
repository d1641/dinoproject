from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404

from olanrewajuscrumy.models import ScrumyGoal, GoalStatus
from django.contrib.auth.models import User

import numpy
import random


# Create your views here.


def index(request):
    filtered = ScrumyGoal.objects.filter(goal_name='Learn Django')

    return HttpResponse(filtered)


def move_goal(request, goal_id):
    context = {
        'error': 'A record with that goal id does not exist'
    }

    try:
        filtered = ScrumyGoal.objects.get(goal_id=goal_id).goal_name

    except ObjectDoesNotExist:
        return render(request, 'olanrewajuscrumy/exception.html', context)

    else:
        return HttpResponse(filtered)


def add_goal(request):
    goal_status = GoalStatus.objects.get(status_name='Weekly Goal')

    user = User.objects.get(username='Louis')
    user.first_name = 'Louis'
    user.last_name = '0ma'
    user.save()

    # picks at random from 1000 and 9999, 10 elements x 2
    id_step1 = numpy.random.randint(low=1000, high=9999, size=10)
    id_step2 = numpy.random.randint(low=1000, high=9999, size=10)

    # joins id_step1 and id_step2 together to make a single array of 20 elements
    id_step3 = numpy.concatenate((id_step1, id_step2), axis=None)

    # shuffles id_step3 to change the positions of it elements
    numpy.random.shuffle(id_step3)

    # picks an element at random to become our goal_id
    goal_id = random.choice(id_step3)

    ScrumyGoal.objects.create(goal_name='Keep Learning Django',
                              goal_id=goal_id,
                              created_by='Louis',
                              moved_by='Louis',
                              owner='Louis',
                              goal_status=goal_status,
                              user=user
                              )
    return HttpResponse('new object created')


def home(request):
    # filtered = ScrumyGoal.objects.filter(goal_name='Keep Learning Django')
    # response = ""
    #
    # for i in filtered:
    #     response += i.goal_name + "<br>"
    # return HttpResponse(response)

    scrumygoal = ScrumyGoal.objects.get(goal_name='Learn Django')
    scrumygoal_name = scrumygoal.goal_name
    scrumygoal_id = scrumygoal.goal_id
    scrumygoal_user = scrumygoal.user

    # all existing users on the User model
    users = User.objects.all()

    # every goal that falls under Weekly Goals
    weeklygoal = GoalStatus.objects.get(status_name='Weekly Goal')
    all_weeklygoal = weeklygoal.scrumygoal_set.all()

    # every goal that falls under Daily Goals
    dailygoal = GoalStatus.objects.get(status_name='Daily Goal')
    all_dailygoal = dailygoal.scrumygoal_set.all()

    # every goal that falls under Verify Goals
    verifygoal = GoalStatus.objects.get(status_name='Verify Goal')
    all_verifygoal = verifygoal.scrumygoal_set.all()

    # every goal that falls under Done Goals
    donegoal = GoalStatus.objects.get(status_name='Done Goal')
    all_donegoal = donegoal.scrumygoal_set.all()


    context = {
        'goal_name': scrumygoal_name,
        'goal_id': scrumygoal_id,
        'user': scrumygoal_user,
        'users': users,
        'all_weeklygoal': all_weeklygoal,
        'all_dailygoal': all_dailygoal,
        'all_verifygoal': all_verifygoal,
        'all_donegoal': all_donegoal,
    }

    return render(request, 'olanrewajuscrumy/home.html', context)
