# olanrewajuscrumy

This application currently renders a table of weekly, daily, verify and done goals.

# Quick start

0. Navigate to your project folder and do pip install --user django-olanrewajuscrumy/dist/django-olanrewajuscrumy-0.0.0.tar.gz to install this application

1. Add "olanrewajuscrumy" to your INSTALLED_APPS settings like this::

          INSTALLED APPS = [
                    ...
                    'olanrewajuscrumy',
          ]
          
2. Include the olanrewajuscrumy URLconf in your project urls.py like this::

          path('olanrewajuscrumy/', include('olanrewajuscrumy.urls')),
          
3. Run `python manage.py migrate` to create models.

4. Start the development server and visit http://127.0.0.1:8080/olanrewajuscrumy/ to be welcomed to django. :)
