import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

    # allows setup.py to be runnable from any path
    os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

    setup(
            name='django-olanrewajuscrumy',
            packages=find_packages(),
            include_package_data=True,
            license='BSD License',
            long_description=README,
            author='Olanrewaju Alawode',
            author_email='oa211048@gmail.com',
            classifiers=[
                'Enviroment :: Web Enviroment',
                'Framework :: Django',
                'Intended Audience :: Everyone',
                'License :: OSI Approved :: BSD License',
                'Operating System :: OS Independent',
                'Programming Language :: Python',
                ],
            )
