from django.urls import path, include

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('<int:goal_id>/', views.move_goal, name='move_goal'),
    path('add_goal/', views.add_goal, name='add_goal'),
    path('home/', views.home, name='home'),

    path('accounts/', include('django.contrib.auth.urls')),
]