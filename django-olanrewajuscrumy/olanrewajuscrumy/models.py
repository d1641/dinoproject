from django.db import models

from django.contrib.auth.models import User


# Create your models here.

# store all possible status of a goal
class GoalStatus(models.Model):
    status_name = models.CharField(max_length=50)

    def __str__(self):
        return self.status_name


# keeps record of each goals
class ScrumyGoal(models.Model):
    goal_name = models.CharField(max_length=50)
    goal_id = models.IntegerField(primary_key=True, default=1, editable=False)
    created_by = models.CharField(max_length=50)
    moved_by = models.CharField(max_length=50)
    owner = models.CharField(max_length=50)
    goal_status = models.ForeignKey(GoalStatus, on_delete=models.PROTECT)
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    def __str__(self):
        return self.goal_name


# stores information about activities/actions carried out on a goal
class ScrumyHistory(models.Model):
    moved_by = models.CharField(max_length=50)
    created_by = models.CharField(max_length=50)
    moved_from = models.CharField(max_length=50)
    moved_to = models.CharField(max_length=50)
    time_of_action = models.DateTimeField(auto_now_add=True, null=True)
    goal = models.ForeignKey(ScrumyGoal, on_delete=models.PROTECT)

    def __str__(self):
        return self.created_by
